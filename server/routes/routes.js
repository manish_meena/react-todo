const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const Todo = require("../../models/schema");

router.get("/", function(req, res) {
  res.render("index");
});

router.route("/todos").post(function(req, res) {
  var todo = new Todo();
  todo.text = req.body.text;
  todo.isChecked = req.body.isChecked;
  todo._id = req.body._id;
  todo.save(function(err) {
    if (err) res.send(err);
    res.send("Task successfully added!");
  });
});

router.route("/todos").get(function(req, resp) {
  Todo.find({}).then(function(data) {
    resp.send(data);
  });
});

// router.route("/deletetodos/:id").get(function(req, resp) {
//     console.log(req.params.id);
//   console.log("hhhhhhh")
  // Todo.findByIdAndRemove({_id:req.params.id}).then(function(data) {
  //   resp.send(data);
  // });
// });

router.delete("/todos/:id",function(req,res){
  Todo.findByIdAndRemove({_id:req.params.id}).then(function(data) {
    resp.send(data);
  });
});

router.put("/todos/:id",function(req, resp) {
  Todo.findByIdAndUpdate({_id:req.params.id},req.body).then(function(data) {
    resp.send(data);
  });
});

module.exports = router;
