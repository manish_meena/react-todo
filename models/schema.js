const mongoose = require('mongoose');

var TodoSchema = new mongoose.Schema({
    text : String,
    isChecked : Boolean,
    _id : String
})

module.exports = mongoose.model('Todo',TodoSchema);